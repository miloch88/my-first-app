import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-servers',
  templateUrl: './servers.component.html',
  styleUrls: ['./servers.component.css']
})
export class ServersComponent implements OnInit {
  allowNewServer = false;
  resetUserName = false;
  serverCreation = "No server was created";
  serverCreated = false;
  serverName = "";
  userName = "";
  servers = ['TestServer', 'TestServer2']
  display = false;
  clicks = [];
  number = 0;
  isFifth = false;

  constructor() {
    setTimeout(()=>{this.allowNewServer = true;},2000);
   }

  ngOnInit(): void {
  }

  onCreateServer(){
    this.serverCreated = true;
    this.servers.push(this.serverName);
    this.serverCreation = "Server was created! Name is " + this.serverName;
  }

  onUpdateServerName(event:Event){
    this.serverName = (<HTMLInputElement>event.target).value;
  }

  resetName(){
    this.userName = "";
  }

  changeDisplay(){
    this.display = !this.display;
    this.clicks.push(new Date());

  }

}
